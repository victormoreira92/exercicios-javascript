const booksByCategory = [
    {
    category: "Riqueza",
        books: [
            {
            title: "Os segredos da mente milionária",
            author: "T. Harv Eker",
            },
            {
            title: "O homem mais rico da Babilônia",
            author: "George S. Clason",
            },
            {
            title: "Pai rico, pai pobre",
            author: "Robert T. Kiyosaki e Sharon L. Lechter",
            },
        ],
    },
    {
    category: "Inteligência Emocional",
        books: [
            {
            title: "Você é Insubstituível",
            author: "Augusto Cury",
            },
            {
            title: "Ansiedade – Como enfrentar o mal do século",
            author: "Augusto Cury",},
            {
            title: "Os 7 hábitos das pessoas altamente eficazes",
            author: "Stephen R. Covey"
            }
        ]
    }
];



function mostrarNumeroAutores(json){
    let listaAutores = json.map((ele) => ele.books.map((ele)=> ele.author));
    let qntdeAutores = {}
    for (const lista of listaAutores) {
            for(let autor of lista){
                qntdeAutores[autor] = qntdeAutores[autor] ? qntdeAutores[autor] + 1 : 1;
            }
    }
    console.log("A quantidade de autores é "+Object.keys(qntdeAutores).length)
}
mostrarNumeroAutores(booksByCategory)