const booksByCategory = [
    {
    category: "Riqueza",
        books: [
            {
            title: "Os segredos da mente milionária",
            author: "T. Harv Eker",
            },
            {
            title: "O homem mais rico da Babilônia",
            author: "George S. Clason",
            },
            {
            title: "Pai rico, pai pobre",
            author: "Robert T. Kiyosaki e Sharon L. Lechter",
            },
        ],
    },
    {
    category: "Inteligência Emocional",
        books: [
            {
            title: "Você é Insubstituível",
            author: "Augusto Cury",
            },
            {
            title: "Ansiedade – Como enfrentar o mal do século",
            author: "Augusto Cury",},
            {
            title: "Os 7 hábitos das pessoas altamente eficazes",
            author: "Stephen R. Covey"
            }
        ]
    }
];



function numeroCategoriasELivros(json){
    // Lista com as categorias dos livros
    let categoriasLivrosLista = json.map((data)=> data.category)

    //Lista de objetos com os livros e autores dividido pelas categorias  
    let todosLivroseAutores = categoriasLivrosLista.map((ele, index)=>{
        if(json[index].category === ele)
            return json[index].books
    })

    // String contendo a quantidade de categorias 
    let numeroCategoriasQntLivros = `Numero de Categorias: ${categoriasLivrosLista.length}\n`
    
    //Soma dos livros por categoria,  
    categoriasLivrosLista.map((ele, index) =>{
        numeroCategoriasQntLivros += `${ele}: ${todosLivroseAutores[index].length}\n`
    })

    console.log(numeroCategoriasQntLivros)
}

numeroCategoriasELivros(booksByCategory)
