const read = require('readline-sync');

console.log("Digite as três notas: ")

let notas = new Array(3).fill(0).map((element)=> element = read.questionInt())
let ini = 0
mediaAluno(notas)

function  mediaAluno (listaNotas){
   let soma =  listaNotas.reduce((acc, curr) => acc + curr,ini);
   let notaFinal =  parseFloat(soma / listaNotas.length).toFixed(1);

   if(notaFinal >= 6.0){
    console.log("A média do aluno é: "+ notaFinal+" Situação: Aprovado")
   }else{
    console.log("A média do aluno é: "+ notaFinal+" Situação: Reprovado")
   }
}


