const read = require('readline-sync');

var contas = {
    receitas:[],
    despesas:[]
}

function registrarMovimentacaoes(){
    let numeroDespesas = read.question("Qual o numero de despesas que vai resgistrar? ");
    let numeroReceitas = read.question("Agora, digite o número receitas que vai registrar?");
    
    console.log("Digite o valor da suas despesas: ")
    let count = 1;
    while(count <= numeroDespesas){
        contas.despesas.push(read.questionFloat("Despesa nº "+count+" "))
        count++
    }

    count=1;
    console.log("")
    console.log("Agora, digite as suas receitas")
    while(count <= numeroReceitas){
        contas.receitas.push(read.questionFloat("Receitas nº "+count+" "))
        count++
    }
}

function mostrarSaldoFamilia(){
    let totalDespesas = somarMovimentacaoes(contas.despesas)
    let totalReceitas = somarMovimentacaoes(contas.receitas)
    let saldo = (totalReceitas - totalDespesas).toFixed(2)
    console.log('')
    
    if(saldo < 0){
        console.log("Saldo Negativo: "+saldo)
    }else{
        console.log("saldo Positivo: "+saldo)
    }
    

}

function somarMovimentacaoes(lista){
    let valorInicial = 0;
    return lista.reduce((acc, valorAtual)=> acc + valorAtual,valorInicial)
}

registrarMovimentacaoes();
mostrarSaldoFamilia();