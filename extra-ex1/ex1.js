const read = require('readline-sync');

let listaValores =  new Array(6).fill(0).map(()=> {
    let num = read.question()
    if(num > 0) {
        return parseInt(num)
    }
}).filter(e => e !== undefined)


console.log(listaValores)