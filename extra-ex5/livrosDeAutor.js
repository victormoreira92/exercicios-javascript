const booksByCategory = [
    {
    category: "Riqueza",
        books: [
            {
            title: "Os segredos da mente milionária",
            author: "T. Harv Eker",
            },
            {
            title: "O homem mais rico da Babilônia",
            author: "George S. Clason",
            },
            {
            title: "Pai rico, pai pobre",
            author: "Robert T. Kiyosaki e Sharon L. Lechter",
            },
        ],
    },
    {
    category: "Inteligência Emocional",
        books: [
            {
            title: "Você é Insubstituível",
            author: "Augusto Cury",
            },
            {
            title: "Ansiedade – Como enfrentar o mal do século",
            author: "Augusto Cury",},
            {
            title: "Os 7 hábitos das pessoas altamente eficazes",
            author: "Stephen R. Covey"
            }
        ]
    }
];



function mostrarLivrosAutor(json, nomeAutor){
    let livrosAutor = [];
    json.map((ele) => ele.books.forEach(element =>{
        if(element.author == nomeAutor){
            livrosAutor.push(element);
        }
    }));
    console.log("Os livros "+nomeAutor+" são "+livrosAutor.map((ele)=> ele.title+" "));
}
mostrarLivrosAutor(booksByCategory, 'George S. Clason')