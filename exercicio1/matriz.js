function multiplicacaoMatriz(m1, m2){
    var resultado = [];
    for (var i = 0; i < m1.length; i++) {
        resultado[i] = [];
        for (var j = 0; j < m2[0].length; j++) {
            var soma = 0;
            for (var k = 0; k < m1[0].length; k++) {
                soma += m1[i][k] * m2[k][j];
            }
            resultado[i][j] = soma;
        }
    }
    return resultado;
}

console.log(multiplicacaoMatriz([[2,-1],[2,0]],[[2,3],[-2,1]]))